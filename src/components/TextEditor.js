import React, { useState, useEffect } from "react";
import RichTextEditor from "react-rte";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export default function TextEditor({
  instance,
  addCommentToInstance,
  prevValue,
}) {
  const [value, setValue] = useState(RichTextEditor.createEmptyValue());
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    if (prevValue) {
      setValue(RichTextEditor.createValueFromString(prevValue, "html"));
    }
  }, []);

  const onChange = (value) => {
    setValue(value);

    addCommentToInstance(instance, value.toString("html"));
  };
  if (prevValue) {
    console.log(
      "---prevValue",
      RichTextEditor.createValueFromString(prevValue, "html")
    );
  }

  console.log("---value", value);
  return (
    <div>
      <Button onClick={handleOpen}>Comment</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h4">
            Add comment for <b>{instance}</b>
          </Typography>
          <br />
          <RichTextEditor value={value} onChange={onChange} />
        </Box>
      </Modal>
    </div>
  );
}
