import React from "react";
import { CSVLink } from "react-csv";
import Button from "@mui/material/Button";
import UploadFileIcon from "@mui/icons-material/UploadFile";

const ExportToCSV = ({ mappedData }) => {
  const headers = [
    { label: "Instance ID", key: "instanceID" },
    { label: "Cost Saving ($)", key: "costSaving" },
    { label: "Comments", key: "comments" },
  ];

  return (
    <CSVLink
      data={mappedData}
      headers={headers}
      style={{
        textDecoration: "none",
        float: "right",
        marginTop: 20,
        marginBottom: 20,
        marginRight: 20,
      }}
    >
      <Button variant="contained" color="secondary">
        <UploadFileIcon /> &nbsp;&nbsp;Export Cost Saving Report
      </Button>
    </CSVLink>
  );
};

export default ExportToCSV;
