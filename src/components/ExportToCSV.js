import React from "react";
import { CSVLink } from "react-csv";
import Button from "@mui/material/Button";
import UploadFileIcon from "@mui/icons-material/UploadFile";

const ExportToCSV = ({ mappedData }) => {
  const headers = [
    { label: "Instance ID", key: "instanceID" },
    { label: "Current Type", key: "currentInstanceType" },
    { label: "No Of Days", key: "noOfDaysUsed" },
    { label: "Max CPU Utilization", key: "maxUtilazationValue" },
    { label: "Median CPU Utilization", key: "medianValue" },
    { label: "Recommended Type", key: "recommendedInstanceType" },
    { label: "Cost Saving", key: "costSaving" },
    { label: "Comments", key: "comments" },
  ];

  return (
    <CSVLink
      data={mappedData}
      headers={headers}
      style={{
        textDecoration: "none",
        float: "right",
        marginTop: 20,
        marginBottom: 20,
      }}
    >
      <Button variant="contained">
        <UploadFileIcon /> &nbsp;&nbsp;Export data
      </Button>
    </CSVLink>
  );
};

export default ExportToCSV;
