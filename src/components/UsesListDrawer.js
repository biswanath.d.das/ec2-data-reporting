import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import DailyReportTable from "./DailyReportTable";
import BarChat from "../components/BarChat";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import _ from "lodash";

export default function UsesListDrawer({ instanceID, data }) {
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });
  const [value, setValue] = React.useState("1");
  const [appData, setAppData] = React.useState(data);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const filterTop5Data = () => {
    const shortedData = _.sortBy(data, ["CPUUtilization"]);
    const getFilterData = shortedData.slice(
      Math.max(shortedData.length - 5, 0)
    );
    const reverseData = _.reverse(getFilterData);
    setAppData(reverseData);
  };

  const filterBottom5Data = () => {
    const shortedData = _.sortBy(data, ["CPUUtilization"]);
    const getFilterData = shortedData.slice(0, 5);
    setAppData(getFilterData);
  };

  const shortByHTL = () => {
    const shortedData = _.sortBy(data, ["CPUUtilization"]);
    const reverseData = _.reverse(shortedData);
    setAppData(reverseData);
  };

  const shortByLTH = () => {
    const shortedData = _.sortBy(data, ["CPUUtilization"]);
    setAppData(shortedData);
  };

  const resrtFilter = () => {
    setAppData(data);
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 800 }}
      role="presentation"
      // onClick={toggleDrawer(anchor, false)}
      // onKeyDown={toggleDrawer(anchor, false)}
    >
      <center>
        <p>
          CPU Utilization <b>{instanceID}</b>
        </p>
      </center>

      <Box sx={{ width: "100%", typography: "body1" }}>
        <div style={{ float: "right", marginRight: 10 }}>
          <Stack spacing={2} direction="row">
            <Button
              variant="contained"
              onClick={() => filterTop5Data()}
              size="small"
            >
              <FilterAltIcon /> &nbsp;&nbsp;TOP 5
            </Button>
            <Button
              variant="contained"
              onClick={() => filterBottom5Data()}
              size="small"
            >
              {" "}
              <FilterAltIcon /> &nbsp;&nbsp;BOTTOM 5
            </Button>
            <Button
              variant="contained"
              onClick={() => shortByHTL()}
              size="small"
              color="secondary"
            >
              {" "}
              high to low
            </Button>
            <Button
              variant="contained"
              onClick={() => shortByLTH()}
              size="small"
              color="secondary"
            >
              {" "}
              low to high
            </Button>
            <Button
              variant="outlined"
              onClick={() => resrtFilter()}
              size="small"
            >
              {" "}
              <FilterAltIcon /> &nbsp;&nbsp;RESET
            </Button>
          </Stack>
        </div>
        <br />
        <br />
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example">
              <Tab label="Chart View" value="1" />
              <Tab label="List View" value="2" />
            </TabList>
          </Box>
          <TabPanel value="1">
            <BarChat allData={appData} instanceID={instanceID} />
          </TabPanel>
          <TabPanel value="2">
            {" "}
            <DailyReportTable allData={appData} />
          </TabPanel>
        </TabContext>
      </Box>
    </Box>
  );

  return (
    <div>
      {["right"].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}>View</Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
