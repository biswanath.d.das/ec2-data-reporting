import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function BasicGrid({ costs = [] }) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Item>
            <Card
              variant="outlined"
              style={{
                background: "linear-gradient(#4527a0,rgb(94 53 177 / 60%))",
                border: "none",
                color: "white",
              }}
            >
              <CardContent>
                <Typography sx={{ fontSize: 14 }} gutterBottom>
                  <b> Current Total Cost</b>
                </Typography>
                <Typography variant="h5" component="div">
                  <b> ${parseFloat(costs[0]).toFixed(2)}</b>
                </Typography>
              </CardContent>
            </Card>
          </Item>
        </Grid>
        <Grid item xs={4}>
          <Item>
            <Card
              variant="outlined"
              style={{
                background: "linear-gradient(#1565c0,rgb(21 101 192 / 60%))",
                border: "none",
                color: "white",
              }}
            >
              <CardContent>
                <Typography sx={{ fontSize: 14 }} gutterBottom>
                  <b>Total Recomanded Cost</b>
                </Typography>
                <Typography variant="h5" component="div">
                  <b>${parseFloat(costs[1]).toFixed(2)}</b>
                </Typography>
              </CardContent>
            </Card>
          </Item>
        </Grid>
        <Grid item xs={4}>
          <Item>
            <Card
              variant="outlined"
              style={{
                background: "linear-gradient(#12b76a,rgb(18 183 106 / 60%))",
                border: "none",
                color: "white",
              }}
            >
              <CardContent>
                <Typography sx={{ fontSize: 14 }} gutterBottom>
                  <b>Total Cost Savings</b>
                </Typography>
                <Typography variant="h5" component="div">
                  <b>${parseFloat(costs[2]).toFixed(2)}</b>
                </Typography>
              </CardContent>
            </Card>
          </Item>
        </Grid>
      </Grid>
    </Box>
  );
}
