import * as React from "react";
import { styled } from "@mui/material/styles";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function DailyReportTable({ allData }) {
  return (
    <div>
      <StyledTableRow key={1}>
        <StyledTableCell align="left" style={{ width: 400 }}>
          <b>Date</b>
        </StyledTableCell>
        <StyledTableCell align="left" style={{ width: 400 }}>
          <b>CPU Utilization</b>
        </StyledTableCell>
      </StyledTableRow>
      {allData.map((row) => (
        <StyledTableRow key={row.Label}>
          <StyledTableCell align="left" style={{ width: 400 }}>
            {/* {format(parseISO(row.Label), "MM/dd/yyyy")} */}
            {/* {format(parseISO("01/02/2023 0:00"), "MM/dd/yyyy")} */}
            {row.Label}
          </StyledTableCell>
          <StyledTableCell align="left" style={{ width: 400 }}>
            {row.CPUUtilization}
          </StyledTableCell>
        </StyledTableRow>
      ))}
    </div>
  );
}
