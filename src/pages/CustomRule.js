import React, { useState, useEffect, useContext } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import _ from "lodash";
import { AppData } from "../App";
import TextField from "@mui/material/TextField";
import UsesListDrawer from "../components/UsesListDrawer";
import priceData from "../mock/priceData.JSON";
import ExportToCSV from "../components/ExportToCSV";
import ExportCostReport from "../components/ExportCostReport";
import CostCards from "../components/CostCards";
import TextEditor from "../components/TextEditor";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

export default function CustomizedTables() {
  const [allData, setAllData] = useState([]);
  const [uniqueInstance, setUniqueInstance] = useState([]);
  const value = useContext(AppData);

  useEffect(() => {
    const findUniqueInstance = _.uniq(_.map(value?.appData, "Instance ID"));
    const filterFindUniqueInstance = findUniqueInstance.filter(
      (val) => val != "" && val != undefined
    );
    setUniqueInstance(filterFindUniqueInstance);
  }, [value?.appData]);

  const filterInstanceData = (instance) => {
    const filterInstancesData = value?.appData?.filter(
      (val) => val["Instance ID"] == instance
    );

    return filterInstancesData;
  };

  const getAvarageCPUUtilization = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    // const filterInstancesData = _.groupBy(data, `Instance ID`);
    let totalSumCPUUtilization = 0.0;
    getInstancesData?.map((value) => {
      if (value?.CPUUtilization) {
        totalSumCPUUtilization += Number(value?.CPUUtilization);
      }
    });

    return totalSumCPUUtilization / getInstancesData.length;
  };

  const getTotalMaxCPUUtilization = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    let totalSumCPUUtilization = 0.0;
    getInstancesData?.map((value) => {
      if (value["CPU Max"]) {
        totalSumCPUUtilization += Number(value["CPU Max"]);
      }
    });

    return totalSumCPUUtilization;
  };

  const getTotalMinCPUUtilization = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    let totalSumCPUUtilization = 0.0;
    getInstancesData?.map((value) => {
      if (value["CPU Min"]) {
        totalSumCPUUtilization += Number(value["CPU Min"]);
      }
    });

    return totalSumCPUUtilization;
  };

  const getTotalCPUUtilization = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    let totalSumCPUUtilization = 0.0;
    getInstancesData?.map((value) => {
      if (value?.CPUUtilization) {
        totalSumCPUUtilization += Number(value?.CPUUtilization);
      }
    });

    return totalSumCPUUtilization;
  };

  const totalNoOfDays = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    return getInstancesData.length;
  };

  const getCurrentInstanceType = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    return getInstancesData[0]["Instance Type"];
  };

  const getMedianValue = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    const shortedData = _.sortBy(getInstancesData, ["CPUUtilization"]);
    const mid = Math.floor(getInstancesData.length / 2);
    const getMedianIndex =
      getInstancesData.length % 2 !== 0
        ? shortedData[mid].CPUUtilization
        : (Number(shortedData[mid - 1].CPUUtilization) +
            Number(shortedData[mid].CPUUtilization)) /
          2;
    return getMedianIndex;
  };

  const recommendation = (instance, maxCPUUtilization) => {
    const getInstancesData = filterInstanceData(instance);
    const currentInstanceType = getInstancesData[0]["Instance Type"];
    const findIdOfCurrentInstance = priceData?.find(
      (val) => val?.name === currentInstanceType
    );
    const currentId = findIdOfCurrentInstance?.id;
    // currentId is the id of priceData that is used as a index to find value
    if (Number(maxCPUUtilization) > 0.7) {
      return priceData[currentId]?.name;
    } else if (Number(maxCPUUtilization) < 0.3) {
      return priceData[currentId - 2]?.name;
    } else {
      return priceData[currentId - 1]?.name;
    }
  };

  const getCostOfInstance = (instanceName) => {
    const getInstanceData = priceData?.find((val) => val.name === instanceName);
    return getInstanceData?.price;
  };

  const costSaving = (instance, maxCPUUtilization) => {
    const currentInstantType = getCurrentInstanceType(instance);
    const getCostOfCurrentInstance = getCostOfInstance(currentInstantType);
    const getRecomandedInstance = recommendation(instance, maxCPUUtilization);
    const getCostOfRecomandedInstance = getCostOfInstance(
      getRecomandedInstance
    );

    return (
      Number(getCostOfCurrentInstance) - Number(getCostOfRecomandedInstance)
    );
  };

  const getMaxUtilazationValue = (instance) => {
    const getInstancesData = filterInstanceData(instance);
    const shortedData = _.sortBy(getInstancesData, ["CPUUtilization"]);
    const getFilterData = shortedData[shortedData.length - 1]?.CPUUtilization;
    return getFilterData;
  };

  useEffect(() => {
    const getCalclulatedRawData = localStorage.getItem("calclulatedData");
    const getCalclulatedData = JSON.parse(getCalclulatedRawData);

    if (getCalclulatedData?.length > 1) {
      console.info("Calculated data getting from local store");
      setAllData(shortData(getCalclulatedData));
    } else {
      console.log("CalculatingData");
      const mappedData = uniqueInstance?.map((instance) => {
        return {
          instanceID: instance,
          currentInstanceType: getCurrentInstanceType(instance),
          noOfDaysUsed: totalNoOfDays(instance),
          totalCPUUtilization: getTotalCPUUtilization(instance),
          totalMaxCPUUtilization: getTotalMaxCPUUtilization(instance),
          totalMinCPUUtilization: getTotalMinCPUUtilization(instance),
          medianValue: getMedianValue(instance),
          avarageCPUUtilization: getAvarageCPUUtilization(instance),
          dailyUse: filterInstanceData(instance),
          recommendedInstanceType: recommendation(
            instance,
            getMaxUtilazationValue(instance)
          ),
          costSaving: costSaving(instance, getMaxUtilazationValue(instance)),
          maxUtilazationValue: getMaxUtilazationValue(instance),
        };
      });
      setAllData(shortData(mappedData));
      localStorage.setItem("calclulatedData", JSON.stringify(mappedData));
    }
  }, [uniqueInstance]);

  const addCommentToInstance = (instance, inputDataHtml) => {
    const getInstancesData = allData?.find(
      (val) => val?.instanceID === instance
    );
    const filterData = allData?.filter((val) => val?.instanceID != instance);
    getInstancesData.comments = inputDataHtml;
    filterData.push(getInstancesData);
    setAllData(shortData(filterData));
    localStorage.setItem("calclulatedData", JSON.stringify(filterData));
  };

  const shortData = (data) => {
    const shortedData = _.sortBy(data, ["totalCPUUtilization"]);

    return shortedData;
  };

  const calCulateTotalSavings = () => {
    if (allData?.length) {
      const savings = allData?.reduce((total, data) => {
        return Number(total?.costSaving) + Number(data.costSaving);
      });

      const currentTotalCost = allData?.reduce((total, data) => {
        return (
          Number(getCostOfInstance(total?.currentInstanceType)) +
          Number(getCostOfInstance(data.currentInstanceType))
        );
      });

      const recomandedTotalCost = allData?.reduce((total, data) => {
        return (
          Number(getCostOfInstance(total?.recommendedInstanceType)) +
          Number(getCostOfInstance(data.recommendedInstanceType))
        );
      });

      return [currentTotalCost, recomandedTotalCost, savings];
    }
  };
  return (
    <div style={{ margin: 10 }}>
      <CostCards costs={calCulateTotalSavings()} />

      <ExportToCSV mappedData={allData} />
      <ExportCostReport mappedData={allData} />
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Account
              </StyledTableCell>
              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Instance IDs
              </StyledTableCell>
              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Current Type
              </StyledTableCell>

              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                No Of Days
              </StyledTableCell>

              <StyledTableCell align="left" style={{ fontSize: 12 }}>
                CPU Utilization Values
              </StyledTableCell>

              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Max CPU Utilization
              </StyledTableCell>
              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Recommended Type
              </StyledTableCell>
              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Cost Saving&nbsp;($)
              </StyledTableCell>
              <StyledTableCell align="center" style={{ fontSize: 12 }}>
                Comments
              </StyledTableCell>
              <StyledTableCell align="right" style={{ fontSize: 12 }}>
                Usage
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {allData?.map((data) => (
              <StyledTableRow key={data?.instanceID}>
                <StyledTableCell align="right">
                  {" "}
                  whiteboard-back-end-partnerships
                </StyledTableCell>
                <StyledTableCell align="right">
                  {" "}
                  {data?.instanceID}
                </StyledTableCell>
                <StyledTableCell align="right">
                  <p style={{ color: "red" }}>{data?.currentInstanceType}</p>
                  <small>
                    (Cost: $
                    {parseFloat(
                      getCostOfInstance(data?.currentInstanceType)
                    ).toFixed(2)}
                    )
                  </small>
                </StyledTableCell>

                <StyledTableCell align="right">
                  {data?.noOfDaysUsed}
                </StyledTableCell>
                <StyledTableCell align="left" width={200}>
                  <p
                    style={{
                      margin: 1,
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <small> Total Max CPU Utilization :</small>{" "}
                    <b>{parseFloat(data?.totalMaxCPUUtilization).toFixed(3)}</b>
                  </p>
                  <p
                    style={{
                      margin: 1,
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <small>Total Min CPU Utilization :</small>{" "}
                    <b>{parseFloat(data?.totalMinCPUUtilization).toFixed(3)}</b>
                  </p>
                  <p
                    style={{
                      margin: 1,
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <small> Median CPU Utilization :</small>{" "}
                    <b>{parseFloat(data?.medianValue).toFixed(3)}</b>
                  </p>
                  <p
                    style={{
                      margin: 1,
                      display: "flex",
                      justifyContent: "space-between",
                    }}
                  >
                    <small> Total CPU Utilization :</small>{" "}
                    <b>{parseFloat(data?.totalCPUUtilization).toFixed(3)}</b>
                  </p>
                </StyledTableCell>

                <StyledTableCell align="right">
                  <p>{parseFloat(data?.maxUtilazationValue).toFixed(3)}</p>
                  <b>
                    {parseFloat(data?.maxUtilazationValue * 100).toFixed(2)} %
                  </b>
                </StyledTableCell>
                <StyledTableCell align="right">
                  <p style={{ color: "green" }}>
                    {data?.recommendedInstanceType}
                  </p>
                  <small>
                    (Cost: $
                    {parseFloat(
                      getCostOfInstance(data?.recommendedInstanceType)
                    ).toFixed(2)}
                    )
                  </small>
                </StyledTableCell>
                <StyledTableCell align="right">
                  <b
                    style={
                      data?.costSaving > 0
                        ? { color: "green" }
                        : { color: "red" }
                    }
                  >
                    {parseFloat(data?.costSaving).toFixed(2)}
                  </b>
                </StyledTableCell>
                <StyledTableCell align="right">
                  <TextEditor
                    instance={data?.instanceID}
                    addCommentToInstance={addCommentToInstance}
                    prevValue={data?.comments}
                  />
                  {/* <TextField
                    label="Comments"
                    variant="filled"
                    color="success"
                    onChange={(e) => addCommentToInstance(data?.instanceID, e)}
                    defaultValue={data?.comments}
                    focused
                  /> */}
                </StyledTableCell>
                <StyledTableCell align="right">
                  <UsesListDrawer
                    data={data?.dailyUse}
                    instanceID={data?.instanceID}
                  />
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
