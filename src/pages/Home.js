import React, { useState, useEffect, useContext } from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import { Card, Grid } from "@mui/material";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import Papa from "papaparse";
import { CSVLink } from "react-csv";
import _ from "lodash";

import { AppData } from "../App";
const allowedExtensions = ["csv"];

const Mint = () => {
  const [start, setStart] = useState(false);
  const [response, setResponse] = useState(null);
  const [file, setFile] = useState(null);
  const [selectedFile, setSelectedFile] = useState();
  const [preview, setPreview] = useState();
  const [description, setDescription] = useState(null);
  const [data, setData] = useState([]);
  const [outputCsv, setOutputCsv] = useState([]);
  const value = useContext(AppData);

  const handleCsvFileChange = (e) => {
    if (e.target.files.length) {
      const inputFile = e.target.files[0];
      const fileExtension = inputFile?.type.split("/")[1];
      if (!allowedExtensions.includes(fileExtension)) {
        return;
      }
      handleParse(inputFile);
    }
  };

  const handleParse = (inputFile) => {
    const reader = new FileReader();
    reader.onload = async ({ target }) => {
      const csv = Papa.parse(target.result, { header: true });

      let totalSumCPUUtilization = 0.0;
      csv.data.map((value) => {
        if (value?.CPUUtilization) {
          totalSumCPUUtilization += Number(value?.CPUUtilization);
        }
      });

      const parsedData = csv?.data;

      localStorage.setItem("data", JSON.stringify(parsedData));
      value?.setAppData(parsedData);

      history("/custom_rule");
    };
    reader.readAsText(inputFile);
  };

  let history = useNavigate();

  const addDataToBlockchain = async ({ event, category, attributes }) => {
    setStart(true);

    // let results;
    let outputCsvData = [];
    try {
      for (let i = 0; i < data.length; i++) {
        const outputData = {
          ...data[i],
        };

        outputCsvData.push(outputData);
      }
      setOutputCsv(outputCsvData);
      setStart(false);
    } catch (err) {}
  };

  useEffect(() => {
    localStorage.clear();
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }
    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onFileChange = (event) => {
    setFile(event.target.files[0]);
    setSelectedFile(event.target.files[0]);
  };

  const modalClose = () => {
    setStart(false);
    setResponse(null);
    history("/");
  };

  const headers = [
    { label: "Name", key: "Name" },
    { label: "Emp NO", key: "EmpNO" },
    { label: "DOJ", key: "DOJ" },
    { label: "Employee Type", key: "EmployeeType" },
    { label: "Level", key: "Level" },
    { label: "Competency", key: "Competency" },
    { label: "Location", key: "Location" },
    { label: "Award Category", key: "AwardCategory" },
    { label: "Nominating Person", key: "NominatingPerson" },
    { label: "Project", key: "Project" },
    { label: "ImpactStatement", key: "ImpactStatement" },
    { label: "Token ID", key: "token" },
    { label: "Link", key: "link" },
  ];

  return (
    <>
      <div className="form-layer2">
        <Grid container columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
          <Grid item lg={3} md={3} sm={12} xs={12}></Grid>
          <Grid item lg={6} md={6} sm={12} xs={12}>
            <div style={{ margin: 20 }}>
              <Card
                style={{
                  background: "rgb(255 255 255)",
                }}
              >
                <Grid container>
                  {outputCsv?.length > 0 ? (
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                      <div
                        style={{
                          border: "1px solid",
                          margin: 25,
                          padding: 25,
                        }}
                      >
                        <center>
                          <h4>Tokens are readys, please download it</h4>
                          <CSVLink data={outputCsv} headers={headers}>
                            <Button
                              variant="contained"
                              size="medium"
                              type="button"
                              style={{
                                marginTop: 10,
                                textDecoration: "none",
                              }}
                            >
                              Download CSV
                            </Button>
                          </CSVLink>
                          <Button
                            variant="outlined"
                            size="medium"
                            type="button"
                            style={{
                              marginTop: 10,
                              marginLeft: 10,
                              textDecoration: "none",
                              color: "red",
                            }}
                            onClick={() => setOutputCsv([])}
                          >
                            Close
                          </Button>
                        </center>
                      </div>
                    </Grid>
                  ) : (
                    <Grid item lg={12} md={12} sm={12} xs={12}>
                      <div
                        style={{
                          padding: "20px",
                        }}
                      >
                        <h4>UPLOAD</h4>
                        <Formik
                          initialValues={{
                            attributes: [],
                          }}
                          onSubmit={(values, { setSubmitting }) => {
                            addDataToBlockchain(values);
                            setSubmitting(false);
                          }}
                        >
                          {({ touched, errors, isSubmitting, values }) => (
                            <Form>
                              <Grid container>
                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                  <div className="form-group">
                                    <label for="title" className="my-2">
                                      Choose Csv file{" "}
                                      <span className="text-danger">*</span>{" "}
                                      <br />
                                    </label>

                                    <input
                                      className={`form-control text-muted`}
                                      onChange={handleCsvFileChange}
                                      id="csvInput"
                                      name="file"
                                      type="file"
                                      style={{
                                        padding: 15,
                                        margin: 10,
                                        borderRadius: 5,
                                        border: "1px solid #80808091",
                                      }}
                                    />
                                  </div>
                                </Grid>

                                <Grid item lg={12} md={12} sm={12} xs={12}>
                                  <div
                                    className="form-group"
                                    style={{
                                      marginLeft: 10,
                                      marginTop: 10,
                                      float: "right",
                                    }}
                                  >
                                    <span className="input-group-btn">
                                      {/* <Button
                                        variant="contained"
                                        size="large"
                                        sx={{
                                          marginX: "15px",
                                          marginBottom: "15px",
                                        }}
                                        type="submit"
                                        value={"Submit"}
                                        style={{
                                          fontSize: 16,
                                          padding: "10px 24px",
                                          borderRadius: 12,
                                        }}
                                      >
                                        UPLOAD
                                      </Button> */}
                                    </span>
                                  </div>
                                </Grid>
                              </Grid>
                            </Form>
                          )}
                        </Formik>
                      </div>
                    </Grid>
                  )}
                </Grid>
              </Card>
            </div>
          </Grid>
          <Grid item lg={3} md={3} sm={12} xs={12}></Grid>
        </Grid>
      </div>
    </>
  );
};
export default Mint;
