import React, { useState, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import CustomRule from "./pages/CustomRule";

export const AppData = React.createContext("");

function App() {
  const [appData, setAppData] = useState([]);

  useEffect(() => {
    const getData = localStorage.getItem("data");
    if (getData) {
      const parseData = JSON.parse(getData);
      console.log("Store updated");
      setAppData(parseData);
    }
  }, []);
  return (
    <AppData.Provider value={{ appData, setAppData }}>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/custom_rule" element={<CustomRule />} />
      </Routes>
    </AppData.Provider>
  );
}

export default App;
